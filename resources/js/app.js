import './bootstrap';
import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import axios from 'axios';


Vue.use(Vuetify);
Vue.use(axios, Vuex);

const app = new Vue({
    el: '#app',
    //store,
    //router: Routes,
});

export default app;
